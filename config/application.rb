require_relative 'boot'

require 'rails'
require 'active_model/railtie'
require 'active_record/railtie'
require 'action_controller/railtie'
require 'active_job/railtie'

Bundler.require(*Rails.groups)
# Dotenv::Railtie.load if Rails.env.development? || Rails.env.test?

module SergicInternalApi
  class Application < Rails::Application
    config.load_defaults 5.1

    config.api_only = true

    config.autoload_paths += Dir[Rails.root.join('lib').to_s,
                                 Rails.root.join(
                                   'app', 'models', 'concerns'
                                 ).to_s]
  end
end
