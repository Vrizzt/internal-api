Rails.application.routes.draw do
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sergic-sidekiq'
  root to: 'root#index'
end
