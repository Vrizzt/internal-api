if Rails.env.production?
  ENV['SERGIC_API_ROOT_URI'] = "#{Rails.root}/v1"
  ENV['SERGIC_API_BASE_URI'] = "#{Rails.root}/v1/sergic"
end
