custom_errors = {
  '666': { for_human: 'Custom error test',
           symbol: :custom_error_test
         }
}

## add custom error to be used as symbol
custom_errors.each do |code,message|
  Rack::Utils::SYMBOL_TO_STATUS_CODE[message[:symbol]] = Integer(code.to_s)
  Rack::Utils::HTTP_STATUS_CODES[Integer(code.to_s)] = message[:for_human]
end
