# config valid for current version and patch releases of Capistrano
lock "~> 3.10.0"

set :application, "api-sync"
set :repo_url, "git@github.com:sergicdev/eseis-middleware-api.git"

set :rbenv_type, :user # or :system, depends on your rbenv setup
set :rbenv_ruby, '2.4.2'

# in case you want to set ruby version from the file:
# set :rbenv_ruby, File.read('.ruby-version').strip

set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all # default value

# There is a known bug that prevents sidekiq from starting when pty is true on Capistrano 3.
set :pty,  false

set :sidekiq_default_hooks =>  true
set :sidekiq_pid =>  File.join(shared_path, 'tmp', 'pids', 'sidekiq.pid')
set :sidekiq_env =>  fetch(:rack_env, fetch(:rails_env, fetch(:stage)))
set :sidekiq_log =>  File.join(shared_path, 'log', 'sidekiq.log')
set :sidekiq_options =>  nil
set :sidekiq_require => nil
set :sidekiq_tag => nil
set :sidekiq_config => nil
set :sidekiq_queue => ['default,2', 'high,4', 'low,1']
set :sidekiq_timeout =>  10
set :sidekiq_role =>  :app
set :sidekiq_processes =>  1
set :sidekiq_concurrency => 10

set :bundle_roles, :all                                         # this is default
set :bundle_servers, -> { release_roles(fetch(:bundle_roles)) } # this is default
set :bundle_binstubs, -> { shared_path.join('bin') }            # default: nil
set :bundle_gemfile, -> { release_path.join('Gemfile') }      # default: nil
set :bundle_path, -> { shared_path.join('bundle') }             # this is default. set it to nil for skipping the --path flag.
set :bundle_without, %w{development test}.join(' ')             # this is default
set :bundle_flags, '--deployment --quiet'                       # this is default
set :bundle_env_variables, {}                                   # this is default
set :bundle_clean_options, ""                                   # this is default. Use "--dry-run" if you just want to know what gems would be deleted, without actually deleting them

set :passenger_in_gemfile, true
set :passenger_restart_with_touch, true
set :passenger_roles, :app
set :passenger_restart_runner, :sequence
set :passenger_restart_wait, 5
set :passenger_restart_limit, 2
set :passenger_restart_with_sudo, false
set :passenger_environment_variables, {}
set :passenger_restart_command, 'passenger-config restart-app'
set :passenger_restart_options, -> { "#{deploy_to} --ignore-app-not-running" }


