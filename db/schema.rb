# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20_171_012_153_045) do
  # These are extensions that must be enabled in order to support this database
  enable_extension 'plpgsql'

  create_table 'budget_previsionnel', force: :cascade do |t|
    t.datetime 'datemajaudi'
    t.integer 'objversion'
    t.text 'profilmajaudit'
    t.boolean 'supprime'
    t.text 'userconnected'
    t.integer 'montantbudgetlocataire'
    t.integer 'montantbudgetproprietaire'
    t.integer 'montantprochainappel'
    t.integer 'montantrealise'
    t.integer 'montanttheoriqueappele'
    t.integer 'tauxevolutionlastyear'
    t.integer 'batiment_id'
    t.integer 'budget_id'
    t.integer 'cle_repartition_id'
    t.integer 'plan_comptable_id'
  end

  create_table 'dataqueue_eseis_bud_previsionnel', force: :cascade do |t|
    t.bigint 'id_budget_previsionnel', null: false
    t.datetime 'date'
    t.boolean 'supprime'
    t.string 'traite'
    t.index ['id_budget_previsionnel'], name: 'dataqueue_eseis_bud_previsionnel_index'
  end

  create_table 'dataqueue_eseis_ecr_comptable', force: :cascade do |t|
    t.bigint 'id_ecriture_comptable', null: false
    t.datetime 'date'
    t.boolean 'supprime'
    t.string 'traite'
    t.index ['id_ecriture_comptable'], name: 'dataqueue_eseis_ecr_comptable_index'
  end

  create_table 'dataqueue_eseis_exo_comptable', force: :cascade do |t|
    t.bigint 'id_exercice_comptable', null: false
    t.datetime 'date'
    t.boolean 'supprime'
    t.string 'traite'
    t.index ['id_exercice_comptable'], name: 'dataqueue_eseis_exo_comptable_index'
  end

  create_table 'dataqueue_eseis_plan_comptable', force: :cascade do |t|
    t.bigint 'id_plan_comptable', null: false
    t.datetime 'date'
    t.boolean 'supprime'
    t.string 'traite'
    t.index ['id_plan_comptable'], name: 'dataqueue_eseis_plan_comptable_index'
  end

  create_table 'ecriture_comptable_immeuble', force: :cascade do |t|
    t.datetime 'datemajaudit'
    t.integer 'objversion'
    t.text 'profilmajaudit'
    t.boolean 'supprime'
    t.text 'userconnected'
    t.text 'codeecriture'
    t.integer 'credit'
    t.integer 'credithtva'
    t.time 'dateecheance'
    t.time 'dateecriture'
    t.time 'datefacture'
    t.time 'datesaisie'
    t.integer 'debit'
    t.integer 'debithtva'
    t.boolean 'enattente'
    t.text 'libelle'
    t.integer 'montantrecuperation'
    t.text 'numeropiece'
    t.text 'numerofacture'
    t.text 'referencemaya'
    t.text 'situationfacture'
    t.text 'typeecriture'
    t.boolean 'visible'
    t.integer 'chantier_id'
    t.integer 'recuperabilite_id'
    t.integer 'compte_credit_id'
    t.integer 'compte_debit_id'
    t.integer 'fournisseur_id'
    t.integer 'immeuble_id'
    t.integer 'nature_ecriture_id'
    t.integer 'profil_client_id'
    t.integer 'remise_bancaire_id'
    t.integer 'sinistre_id'
    t.integer 'cle_repartition_id'
    t.time 'daterapprochement'
    t.text 'referencereglement'
    t.text 'typereglement'
    t.integer 'fichier_id'
    t.text 'matriculesalarie'
    t.integer 'numenregistrementmaya'
    t.text 'profilcreation'
    t.integer 'tva_id'
    t.integer 'fichier_lad_id'
  end

  create_table 'exercice_comptable', force: :cascade do |t|
    t.time 'datemajaudit'
    t.integer 'objversion'
    t.text 'profilmajaudit'
    t.boolean 'supprime'
    t.text 'userconnected'
    t.time 'datedebutexercice'
    t.time 'datefinexercice'
    t.time 'datelimiteag'
    t.text 'libelle'
    t.integer 'numeroexercice'
    t.integer 'immeuble_id'
    t.time 'datecloturecomptes'
    t.time 'datecspreparatoire'
    t.time 'dateprevueagsaisie'
    t.time 'dateregulcharges'
    t.time 'datesortiecomptes'
    t.text 'profilcloturecomptes'
    t.text 'profilcspreparatiore'
    t.text 'profildateprevueagsaisie'
    t.text 'profilregulcharges'
    t.text 'profilsortiecomptes'
    t.integer 'assemblee_generale_id'
  end

  create_table 'plan_comptable', force: :cascade do |t|
    t.time 'datemajaudit'
    t.integer 'objversion'
    t.text 'profilmajaudit'
    t.boolean 'supprime'
    t.text 'userconnected'
    t.text 'cletantieme'
    t.text 'codebatiment'
    t.text 'codeplantypeofficiel'
    t.integer 'comptedebit'
    t.text 'detaillebudget'
    t.boolean 'isautocreated'
    t.boolean 'isnoticeable'
    t.text 'libellecomptelong'
    t.text 'libellecomptereduit'
    t.text 'libellesynthesecredit'
    t.text 'libellesynthesedebit'
    t.integer 'nombrecaracterecompte'
    t.text 'numcompte'
    t.text 'souscomptedebit'
    t.integer 'type_categorie_depense_id'
    t.integer 'type_observatoire_cnab_id'
    t.integer 'type_recuperabilite_id'
    t.integer 'type_usage_sous_compte_id'
    t.integer 'caracteristique_id'
    t.integer 'immeuble_id'
    t.boolean 'isplanstandard'
    t.integer 'classeducompte'
  end

  add_foreign_key 'dataqueue_eseis_bud_previsionnel', 'budget_previsionnel', column: 'id_budget_previsionnel'
  add_foreign_key 'dataqueue_eseis_ecr_comptable', 'ecriture_comptable_immeuble', column: 'id_ecriture_comptable'
  add_foreign_key 'dataqueue_eseis_exo_comptable', 'exercice_comptable', column: 'id_exercice_comptable'
  add_foreign_key 'dataqueue_eseis_plan_comptable', 'plan_comptable', column: 'id_plan_comptable'
end
