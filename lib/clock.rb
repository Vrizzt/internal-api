# frozen_string_literal: true

require 'clockwork'
require 'clockwork/database_events'
require_relative '../config/boot'
require_relative '../config/environment'

module Clockwork
  Clockwork.manager = DatabaseEvents::Manager.new

  error_handler do |e|
    if e.to_s.include?('PG::ConnectionBad')
      while ActiveRecord::Base.connection.active? == false
        ActiveRecord::Base.establish_connection
        unless ActiveRecord::Base.connection.active?
          Appsignal.send_exception(e)
          sleep 10
        end
      end
    else
      Appsignal.send_exception(e)
    end
  end

  every(15.seconds, 'dataqueue.eseis_account_code') do
    # Dataqueue::EseisAccountCode.update_all(state: :pending)
    Dataqueue::EseisAccountCode.with_state(:pending).ids.each do |id|
      Sync::SyncEseisAccountCodeJob.perform_later(id)
    end
  end

  every(15.seconds, 'dataqueue.eseis_account_place_entry') do
    # Dataqueue::EseisAccountPlaceEntry.update_all(state: :pending)
    Dataqueue::EseisAccountPlaceEntry.with_state(:pending).ids.each do |id|
      Sync::SyncEseisAccountPlaceEntryJob.perform_later(id)
    end
  end

  every(60.seconds, 'dataqueue.eseis_budget') do
    # Dataqueue::EseisBudget.update_all(state: :pending)
    Dataqueue::EseisBudget.with_state(:pending).ids.each do |id|
      Sync::SyncEseisBudgetJob.perform_later(id)
    end
  end

  every(15.seconds, 'dataqueue.eseis_fiscal_year') do
    # Dataqueue::EseisFiscalYear.update_all(state: :pending)
    Dataqueue::EseisFiscalYear.with_state(:pending).ids.each do |id|
      Sync::SyncEseisFiscalYearJob.perform_later(id)
    end
  end
end
