class AppError < StandardError
  attr_reader :context, :tag
  def initialize(
    msg     = 'Default AppError message',
    context = 'no context',
    tag     = {}
  )
    @context = context
    @tag     = tag
    super(msg)
  end
end
