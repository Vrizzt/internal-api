class CrmFlushEseisDataqueueJob < ApplicationJob
  def perform
    Appsignal.monitor_transaction(
      'perform_job.crm_flush_eseis_dataqueue.job',
      class: 'FLUSH',
      method: self.class.name,
      queue_start: Time.zone.now
    ) do
      Dataqueue::EseisAccountCode.delete_all
      Dataqueue::EseisAccountPlaceEntry.delete_all
      Dataqueue::EseisFiscalYear.delete_all
      Dataqueue::EseisBudget.delete_all
    end
    rescue StandardError => e
      Appsignal.set_error(e)
    ensure
      Appsignal::Transaction.complete_current!
  end
end
