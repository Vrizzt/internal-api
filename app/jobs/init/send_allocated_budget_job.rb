module Init
  class SendAllocatedBudgetJob < ApplicationJob
    def perform(crm_allocated_budget)
      Appsignal.monitor_transaction(
        'perform_job.send_allocated_budget.job',
        class: 'SEND',
        method: self.class.name,
        queue_start: Time.zone.now
      ) do
          allocated_budget_object = Eseis::AllocatedBudget.new(
            crm_allocated_budget.budget.fiscal_year.place.id,
            crm_allocated_budget.budget.fiscal_year.id,
            crm_allocated_budget.account_code.numcompte,
            crm_allocated_budget.montantbudgetproprietaire,
            crm_allocated_budget.id
          )
          unless allocated_budget_object.valid?
            Appsignal.set_error(AppError.new(allocated_budget_object.errors.messages.as_json))
            next
          end
          begin
            @@api_client_crm.post(
              "#{ENV['SERGIC_API_BASE_URI']}/allocated_budgets",
              { allocated_budget: allocated_budget_object.as_json }
            )
        rescue RestClient::ExceptionWithResponse => e
          Appsignal.set_error(appsignal_rest_msg(e, crm_allocated_budget))
        ensure
          Appsignal::Transaction.complete_current!
        end
      end
    end
  end
end
