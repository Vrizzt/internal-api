module Init
  class SendFiscalYearJob < ApplicationJob
    def perform(crm_fiscal_year)
      Appsignal.monitor_transaction(
        'perform_job.send_fiscal_year.job',
        class: 'SEND',
        method: self.class.name,
        queue_start: Time.zone.now
      ) do
        fiscal_year_object = Eseis::FiscalYear.new(
          crm_fiscal_year.libelle,
          crm_fiscal_year.datedebutexercice,
          crm_fiscal_year.datefinexercice,
          Time.zone.now >= crm_fiscal_year.datedebutexercice && Time.zone.now <= crm_fiscal_year.datefinexercice,
          crm_fiscal_year.immeuble_id,
          crm_fiscal_year.id
        )
        unless fiscal_year_object.valid?
          Appsignal.set_error(AppError.new(fiscal_year_object.errors.messages.as_json))
          next
        end
        begin
          @@api_client_crm.post(
            "#{ENV['SERGIC_API_BASE_URI']}/fiscal_years",
            fiscal_year: fiscal_year_object.as_json
          )
        rescue RestClient::ExceptionWithResponse => e
          Appsignal.set_error(appsignal_rest_msg(e, crm_fiscal_year))
        ensure
          Appsignal::Transaction.complete_current!
        end
      end
    end
  end
end
