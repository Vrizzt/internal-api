module Init
  class SendAccountPlaceEntryJob < ApplicationJob
    def perform(place_id, crm_account_place_entry)
      Appsignal.monitor_transaction(
        'perform_job.send_allocated_budget.job',
        class: 'SEND',
        method: self.class.name,
        queue_start: Time.zone.now
      ) do
        begin
          sergic_fiscal_year_id = crm_account_place_entry
                                    &.place
                                    &.fiscal_years
                                    &.between(crm_account_place_entry
                                              .dateecriture
                                              .to_s)
                                    &.first
                                    &.id

          amount = if crm_account_place_entry&.debit.to_i.zero?
                     crm_account_place_entry.credit
                   else
                     - crm_account_place_entry.debit
                   end

          account_code =
            ::AccountCode.joins(:place)
                         .where(id: crm_account_place_entry.compte_debit_id, immeuble_id: place_id)
                         .first.numcompte

          attachment_urls = if crm_account_place_entry&.fichier_id.present?
                              if crm_account_place_entry&.fichier_id.to_i != 0
                                "#{ENV['PRIVATE_FILES_URI']}/fichierLad/\
                                #{crm_account_place_entry.fichier_id}"
                              else
                                "#{ENV['PRIVATE_FILES_URI']}/easyfolder/\
                                #{crm_account_place_entry.fichier_id}.pdf"
                              end
                            else
                              ''
                            end

          if crm_account_place_entry.fournisseur_id.present?
            provider_crm = ::Provider.find(crm_account_place_entry.fournisseur_id)
            provider = Eseis::Provider.new(provider_crm).as_json
          else
            provider = {}
          end

          account_place_entry_object = Eseis::AccountPlaceEntry.new(
            provider,
            crm_account_place_entry.libelle,
            crm_account_place_entry.dateecriture,
            amount,
            crm_account_place_entry.id,
            crm_account_place_entry.immeuble_id,
            sergic_fiscal_year_id,
            account_code,
            crm_account_place_entry.fournisseur_id,
            crm_account_place_entry.debit,
            crm_account_place_entry.credit,
            crm_account_place_entry.chantier_id,
            [attachment_urls: [attachment_urls]]
          )
          unless account_place_entry_object.valid?
            Appsignal.set_error(AppError.new(account_place_entry_object.errors.messages.as_json))
            next
          end

          @@api_client_crm.post(
            "#{ENV['SERGIC_API_BASE_URI']}/account_place_entries",
              account_place_entry: account_place_entry_object.as_json
          )
        rescue RestClient::ExceptionWithResponse => e
          Appsignal.set_error(appsignal_rest_msg(e, crm_account_place_entry))
        ensure
          Appsignal::Transaction.complete_current!
        end
      end
    end
  end
end
