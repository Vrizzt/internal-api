# frozen_string_literal: true

module Init
  class PlaceInitJob < ApplicationJob
    def perform(place_id = nil)
      Appsignal.monitor_transaction(
        'perform_job.sync_eseis.account_code.job',
        class: 'SYNC',
        method: self.class.name,
        queue_start: Time.zone.now
      ) do
          place = place_id.nil? ?
            ::Place.where(pack_id: ENV['ESEIS_PACK_ID']) :
            [::Place.find(place_id)]

          place.each do |l|
            l.fiscal_years.each do |crm_fiscal_year|
              Init::SendFiscalYearJob.perform_later(crm_fiscal_year)
            end

            l.account_codes.each do |crm_account_code|
              Init::SendAccountCodeJob.perform_later(crm_account_code)
            end

            filters = { place_id: l.id, budget_state: %w[A V] }
            QueryObject::AllocatedBudgetQuery.call(filters)
                                             .each do |crm_allocated_budget|
              Init::SendAllocatedBudgetJob.perform_later(crm_allocated_budget)
            end

            l.account_place_entries
             .where(supprime: false)
             .each do |crm_account_place_entry|
              Init::SendAccountPlaceEntryJob.perform_later(l.id, crm_account_place_entry)
            end
          end
        end
    rescue StandardError => e
      if e.to_s.include?('PG::ConnectionBad')
        while ActiveRecord::Base.connection.active? == false
          ActiveRecord::Base.establish_connection
          unless ActiveRecord::Base.connection.active?
            Appsignal.send_exception(e)
            sleep 10
          end
        end
      else
        Appsignal.set_error(e)
      end
    ensure
      Appsignal::Transaction.complete_current!
    end
  end
end
