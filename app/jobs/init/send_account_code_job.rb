module Init
  class SendAccountCodeJob < ApplicationJob
    def perform(crm_account_code)
      Appsignal.monitor_transaction(
        'perform_job.send_account_code.job',
        class: 'SEND',
        method: self.class.name,
        queue_start: Time.zone.now
      ) do
          account_code_object = Eseis::AccountCode.new(
            crm_account_code.libellecomptelong,
            crm_account_code.numcompte,
            crm_account_code.id.to_i
          )
          unless account_code_object.valid?
            Appsignal.set_error(AppError.new(account_code_object.errors.messages.as_json))
            next
          end
          begin
            @@api_client_crm.post(
              "#{ENV['SERGIC_API_BASE_URI']}/account_codes",
              {account_code: account_code_object.as_json}
            )
        rescue RestClient::ExceptionWithResponse => e
          Appsignal.set_error(appsignal_rest_msg(e, crm_account_code))
        ensure
          Appsignal::Transaction.complete_current!
        end
      end
    end
  end
end
