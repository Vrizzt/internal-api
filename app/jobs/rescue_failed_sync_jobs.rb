# frozen_string_literal: true

class RescueFailedSyncJobs < ApplicationJob
  def perform(target = 'all')
    if target.include?('account_code') || target.include?('all')
      Dataqueue::EseisAccountCode.with_state(:failed).ids.each do |id|
        SyncEseisAccountCodeJob.perform_later(id)
      end
    end

    if target.include?('account_place_entry') || target.include?('all')
      Dataqueue::EseisAccountPlaceEntry.with_state(:failed).ids.each do |id|
        SyncEseisAccountPlaceEntryJob.perform_later(id)
      end
    end

    if target.include?('allocated_budget') || target.include?('all')
      Dataqueue::EseisAllocatedBudget.with_state(:failed).ids.each do |id|
        SyncEseisAllocatedBudgetJob.perform_later(id)
      end
    end

    if target.include?('fiscal_year') || target.include?('all')
      Dataqueue::EseisFiscalYear.with_state(:failed).ids.each do |id|
        SyncEseisFiscalYearJob.perform_later(id)
      end
    end
  end
end
