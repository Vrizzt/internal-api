# frozen_string_literal: true

require 'eseis_synchronizer'

module Sync
  class SyncEseisFiscalYearJob < ApplicationJob
    def perform(id)
      # payload = {
      #   appsignal_type: 'perform_job',
      #   appsignal_action: 'sync_eseis.fiscal_year.job',
      #   caller_class: self.class.to_s,
      #   table_name: 'FiscalYear',
      #   dataqueue_id: id
      # }
      # EseisSynchronizer.execute(payload)
      @dataqueue = Dataqueue::EseisFiscalYear.find(id)
      unless @dataqueue.refused?
        @dataqueue.action = ''
        crm_fiscal_year = ::FiscalYear.find(@dataqueue.fiscal_year_id)

        @dataqueue.rescue! if @dataqueue.failed?
        @dataqueue.launch! if @dataqueue.pending?

        # just for testing
        unless crm_fiscal_year&.place&.id.to_i == 25203
          return @dataqueue.refuse! if @dataqueue.in_progress?
        end

        if crm_fiscal_year&.place&.pack&.id != Integer(ENV['ESEIS_PACK_ID'])
          @dataqueue.action = 'Place not in Eseis'
          return @dataqueue.refuse! if @dataqueue.in_progress?
        end

        payload = Eseis::FiscalYear.new(
          crm_fiscal_year.libelle,
          crm_fiscal_year.datedebutexercice,
          crm_fiscal_year.datefinexercice,
          Time.zone.now >= crm_fiscal_year.datedebutexercice && Time.zone.now <= crm_fiscal_year.datefinexercice,
          crm_fiscal_year.immeuble_id,
          crm_fiscal_year.id
        ).as_json
        fiscal_year_playload = {
          fiscal_year: payload
        }
        @@api_client_crm.post(
          "#{ENV['SERGIC_API_BASE_URI']}/fiscal_years",
          fiscal_year_playload
        )
        @dataqueue.done! if @dataqueue.in_progress?
      end
    rescue RestClient::ExceptionWithResponse => e
      Appsignal.set_error(appsignal_rest_msg(e, crm_fiscal_year))
      @dataqueue.action = get_whole_error(appsignal_rest_msg(e, crm_fiscal_year))
      @dataqueue.failed!
    rescue StandardError => e
      Appsignal.set_error(e)
      @dataqueue.failed!
    ensure
      Appsignal::Transaction.complete_current!
      @dataqueue.save
    end
  end
end
