# frozen_string_literal: true

module Sync
  class SyncEseisAccountPlaceEntryJob < ApplicationJob
    def perform(id)
      Appsignal.monitor_transaction(
        'perform_job.sync_eseis.account_place_entry.job',
        class: 'SYNC',
        method: self.class.name,
        queue_start: Time.zone.now
      ) do
        @dataqueue = Dataqueue::EseisAccountPlaceEntry.find(id)
        unless @dataqueue.refused?
          @dataqueue.action = ''
          crm_account_place_entry = ::AccountPlaceEntry.find(@dataqueue.account_place_entry_id)

          @dataqueue.rescue! if @dataqueue.failed?
          @dataqueue.launch! if @dataqueue.pending?

          # # just for testing
          unless crm_account_place_entry&.place&.id.to_i == 25203
            return @dataqueue.refuse! if @dataqueue.in_progress?
          end

          if crm_account_place_entry&.place&.pack&.id != Integer(ENV['ESEIS_PACK_ID'])
            @dataqueue.action = 'Place not in Eseis'
            return @dataqueue.refuse! if @dataqueue.in_progress?
          end

          # set provider
          if crm_account_place_entry.fournisseur_id.present?
            provider_crm = ::Provider.find(crm_account_place_entry.fournisseur_id)
            provider = Eseis::Provider.new(provider_crm).as_json
          else
            provider = {}
          end

          # set amount
          amount = if crm_account_place_entry&.debit.to_i.zero?
                     crm_account_place_entry.credit
                   else
                     - crm_account_place_entry.debit
                   end

          # set sergic_fiscal_year_id
          sergic_fiscal_year_id = crm_account_place_entry&.place&.fiscal_years&.between(
            crm_account_place_entry.dateecriture.to_s
          )&.first&.id

          # set account_code
          account_code =
            ::AccountCode.joins(:place)
                         .where(id: crm_account_place_entry.compte_debit_id, immeuble_id: crm_account_place_entry.place.id)
                         .first.numcompte

          # set attachment_urls
          attachment_urls = if crm_account_place_entry&.fichier_id.present?
                              if crm_account_place_entry&.fichier_id.to_i != 0
                                "#{ENV['PRIVATE_FILES_URI']}/fichierLad/\
                                #{crm_account_place_entry.fichier_id}"
                              else
                                "#{ENV['PRIVATE_FILES_URI']}/easyfolder/\
                                #{crm_account_place_entry.fichier_id}.pdf"
                              end
                            else
                              ''
                            end

          payload = Eseis::AccountPlaceEntry.new(
            provider,
            crm_account_place_entry.libelle,
            crm_account_place_entry.dateecriture,
            amount,
            crm_account_place_entry.id,
            crm_account_place_entry.immeuble_id,
            sergic_fiscal_year_id,
            account_code,
            crm_account_place_entry.fournisseur_id,
            crm_account_place_entry.debit,
            crm_account_place_entry.credit,
            crm_account_place_entry.chantier_id,
            [attachment_urls: [attachment_urls]]
          ).as_json

          account_place_entry_playload = {
            account_place_entry: payload
          }
          @@api_client_crm.post(
            "#{ENV['SERGIC_API_BASE_URI']}/account_place_entries",
            account_place_entry_playload
          )
          @dataqueue.done! if @dataqueue.in_progress?
        end
      end
    rescue RestClient::ExceptionWithResponse => e
      # e = set_message(e)
      Appsignal.set_error(appsignal_rest_msg(e, crm_account_place_entry))
      @dataqueue.action = get_whole_error(appsignal_rest_msg(e, crm_account_place_entry))
      @dataqueue.failed!
    rescue StandardError => e
      Appsignal.set_error(e)
      @dataqueue.failed!
    ensure
      Appsignal::Transaction.complete_current!
      @dataqueue.save
    end
  end
end
