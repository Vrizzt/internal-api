# frozen_string_literal: true

module Sync
  class SyncEseisAccountCodeJob < ApplicationJob
    def perform(id)
      Appsignal.monitor_transaction(
        'perform_job.sync_eseis.account_code.job',
        class: 'SYNC',
        method: self.class.name,
        queue_start: Time.zone.now
      ) do
        @dataqueue = Dataqueue::EseisAccountCode.find(id)
        unless @dataqueue.refused?
          @dataqueue.action = ''
          crm_account_code = ::AccountCode.find(@dataqueue.account_code_id)

          @dataqueue.rescue! if @dataqueue.failed?
          @dataqueue.launch! if @dataqueue.pending?

          # just for testing
          unless crm_account_code&.place&.id.to_i == 25203
            return @dataqueue.refuse! if @dataqueue.in_progress?
          end

          if crm_account_code&.place&.pack&.id != Integer(ENV['ESEIS_PACK_ID'])
            @dataqueue.action = 'Place not in Eseis'
            return @dataqueue.refuse! if @dataqueue.in_progress?
          end

          payload = Eseis::AccountCode.new(
            crm_account_code.libellecomptelong,
            crm_account_code.numcompte,
            crm_account_code.id.to_i
          ).as_json

          account_code_playload = {
            account_code: payload
          }

          @@api_client_crm.post(
            "#{ENV['SERGIC_API_BASE_URI']}/account_codes",
            account_code_playload
          )

          @dataqueue.done! if @dataqueue.in_progress?
        end
      end
    rescue RestClient::ExceptionWithResponse => e
      # e = set_message(e)
      Appsignal.set_error(appsignal_rest_msg(e, crm_account_code))
      @dataqueue.action = get_whole_error(appsignal_rest_msg(e, crm_account_code))
      @dataqueue.failed!
    rescue StandardError => e
      Appsignal.set_error(e)
      @dataqueue.failed!
    ensure
      Appsignal::Transaction.complete_current!
      @dataqueue.save
    end
  end
end
