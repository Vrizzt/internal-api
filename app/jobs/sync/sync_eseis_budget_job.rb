# frozen_string_literal: true

module Sync
  class SyncEseisBudgetJob < ApplicationJob
    def perform(id)
      Appsignal.monitor_transaction(
        'perform_job.sync_eseis.budget.job',
        class: 'SYNC',
        method: self.class.name,
        queue_start: Time.zone.now
      ) do
        @dataqueue = Dataqueue::EseisBudget.find(id)
        unless @dataqueue.refused?
          @dataqueue.action = ''
          crm_budget = ::Budget.find(@dataqueue.budget_id)

          @dataqueue.rescue! if @dataqueue.failed?
          @dataqueue.launch! if @dataqueue.pending?

          # just for testing
          unless crm_budget&.fiscal_year&.place&.id.to_i == 25203
            return @dataqueue.refuse! if @dataqueue.in_progress?
          end

          unless crm_budget&.fiscal_year&.place&.pack&.id == Integer(ENV['ESEIS_PACK_ID'])
            @dataqueue.action = 'Place not in Eseis'
            return @dataqueue.refuse! if @dataqueue.in_progress?
          end

          budget_state = crm_budget&.budget_state&.codesituation

          if %w[A V].include? budget_state
            crm_budget.allocated_budgets.each do |crm_allocated_budget|
              payload = Eseis::AllocatedBudget.new(
                crm_allocated_budget.budget.fiscal_year.place.id,
                crm_allocated_budget.budget.fiscal_year.id,
                crm_allocated_budget.account_code.numcompte,
                crm_allocated_budget.montantbudgetproprietaire,
                crm_allocated_budget.id
              ).as_json

              allocated_budget_playload = { allocated_budget: payload }

              @@api_client_crm.post(
                "#{ENV['SERGIC_API_BASE_URI']}/allocated_budgets",
                allocated_budget_playload
              )
            end
          elsif budget_state == '*'
            crm_budget.allocated_budgets.each do |crm_allocated_budget|
              @@api_client_crm.delete(
                "#{ENV['SERGIC_API_BASE_URI']}/allocated_budgets/#{crm_allocated_budget.id}"
              )
            end

          elsif budget_state == 'P'
            @dataqueue.action = 'Budget in P state'
            return @dataqueue.refuse! if @dataqueue.in_progress?
          end

          @dataqueue.done! if @dataqueue.in_progress?
        end
      end
    rescue RestClient::ExceptionWithResponse => e
      # e = set_message(e)
      Appsignal.set_error(appsignal_rest_msg(e, crm_budget))
      @dataqueue.action = get_whole_error(appsignal_rest_msg(e, crm_budget))
      @dataqueue.failed!
    rescue StandardError => e
      Appsignal.set_error(e)
      @dataqueue.failed!
    ensure
      Appsignal::Transaction.complete_current!
      @dataqueue.save
    end
  end
end
