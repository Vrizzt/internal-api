# frozen_string_literal: true

module Sync
  class SyncWholeDataqueueJob < ApplicationJob
    def perform(table_name)
      Appsignal.monitor_transaction(
        'perform_job.sync_whole_dataqueue.job',
        class: 'SYNC',
        method: self.class.name,
        queue_start: Time.zone.now
      ) do
        case table_name
        when 'account_code'
          Dataqueue::EseisAccountCode.with_state(:pending).ids.each do |id|
            # Dataqueue::EseisAccountCode.update_all(state: :pending)
            SyncEseisAccountCodeJob.perform_later(id)
          end
        when 'account_place_entry'
          Dataqueue::EseisAccountPlaceEntry.with_state(:pending).ids.each do |id|
            # Dataqueue::EseisAccountPlaceEntry.update_all(state: :pending)
            SyncEseisAccountPlaceEntryJob.perform_later(id)
          end
        when 'allocated_budget'
          Dataqueue::EseisAllocatedBudget.with_state(:pending).ids.each do |id|
            # Dataqueue::EseisAllocatedBudget.update_all(state: :pending)
            SyncEseisAllocatedBudgetJob.perform_later(id)
          end
        when 'fiscal_year'
          Dataqueue::EseisFiscalYear.with_state(:pending).ids.each do |id|
            # Dataqueue::EseisFiscalYear.update_all(state: :pending)
            SyncEseisFiscalYearJob.perform_later(id)
          end
        end
      end
    rescue StandardError => e
      Appsignal.set_error(e)
      return
    ensure
      Appsignal::Transaction.complete_current!
    end
  end
end
