# frozen_string_literal: true

class ApplicationJob < ActiveJob::Base
  queue_as :default

  @@api_client_crm ||= ::ApiClient.new(
    client_id:      ENV['SERGIC_API_CLIENT_ID'],
    client_secret:  ENV['SERGIC_API_CLIENT_SECRET'],
    scope:          'sergic',
    grant_type:     'client_credentials',
    auth_uri:       "#{ENV['SERGIC_API_ROOT_URI']}/oauth/token"
  )

  private

  def appsignal_rest_msg(e, object)
    # e.message = "#{e.inspect} - sergic_#{object.class.to_s} id: #{object.id} - error : #{JSON.parse(e.response)['error']['message']}"
    e.message = "#{e.inspect} - sergic_#{object.class.to_s} id: #{object.id} - error : #{JSON.parse(e.response)}"
    binding.pry
    e
  end

  def get_whole_error(e)
    "#{JSON.parse(e.response.as_json)['error']['code']} #{JSON.parse(e.response.as_json)['error']['status'].downcase} - #{JSON.parse(e.response.as_json)['error']['message'].join(' - ')}"
  end

  def set_message(e)
    e.message = "#{e.message} -> #{JSON.parse(e.response.as_json)['error']['message'].join(' - ')}"
    e
  end
end
