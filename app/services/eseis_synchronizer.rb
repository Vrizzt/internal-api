class EseisSynchronizer
  def self.execute(args)
    new(args).tap(&:perform_job)
  end

  # arg = {table_name, dataqueu_id}
  def initialize(args)
    Appsignal.monitor_transaction(
      "#{arg[:appsignal_type]}.#{arg[:appsignal_action]}",
      class: 'SYNC',
      method: arg[:caller_class],
      queue_start: Time.zone.now
    ) do

      @dataqueue = active_record.call('Dataqueue', args[:table_name])
                                .find(arg[:dataqueue_id])


      @dataqueue.launch! if @dataqueue.pending?
      @dataqueue.rescue! if @dataqueue.failed?

      playload = {
        "#{args[:table_name]}":
          active_record.call('Eseis', args[:table_name])
                       .new(@dataqueue.send("#{args[:table_name]}_id"))
                       .as_json
      }
      # idéalement faire un :
      # args.client.post(payload)

      # faire la requete post avec api-client en mode module
    end
  rescue StandardError => e
    error = e&.response.present? ? e&.response.to_s : e.to_s
    Appsignal.set_error(error)
    @dataqueue.action = error
    @dataqueue.failed!
    return
  ensure
    @dataqueue.save
    Appsignal::Transaction.complete_current!
  end

  active_record = lambda do |namespace,table_name|
    Object.const_get("#{namespace}::#{table_name}")
  end
end
