module QueryObject
  class AllocatedBudgetQuery
    attr_reader :relation, :filters

    # class << self
    #   delegate :call, to: :new
    # end
    def self.call(filters = {}, relation = ::AllocatedBudget.all)
      new(filters, relation).tap(&:call).relation
    end

    # filter can have the key/value as follow (key are symbols) :
    # place_id/string
    # current_fiscal_year/boolean
    # budget_state/['A', 'V', '*']
    def initialize(filters = {}, relation = ::AllocatedBudget.all)
      @relation     = relation
      @filters      = filters
    end

    def call
      conditions = {
        budget_previsionnel: { supprime: 'false' },
        budget: { supprime: 'false' }
      }

      # place filter
      conditions = conditions.merge(immeuble: { id: @filters[:place_id] }) if @filters.key?(:place_id)

      # situation budget filter
      conditions = conditions.merge(situation_budget: { codesituation: @filters[:budget_state] }) if @filters.key?(:budget_state)

      # main extract
      @relation = @relation.joins(:account_code,
                                  account_code: [:place],
                                  budget: %i[fiscal_year budget_state])
                           .where(conditions)

      # current fiscal year filter
      @relation = @relation.where("'#{Date.today.to_s}' between exercice_comptable.datedebutexercice and exercice_comptable.datefinexercice") if @filters.key?(:current_fiscal_year)
    end
  end
end
