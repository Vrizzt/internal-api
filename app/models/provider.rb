# == Schema Information
#
# Table name: fournisseur
#
#  id                              :integer          not null, primary key
#  datemajaudit                    :datetime
#  objversion                      :integer
#  profilmajaudit                  :string(255)
#  supprime                        :boolean
#  userconnected                   :string(255)
#  adresse                         :string(255)
#  fax                             :string(255)
#  nomcorrespondant                :string(255)
#  nomentreprise                   :string(255)
#  referencemaya                   :string(255)
#  telephone                       :string(255)
#  pole_id                         :integer
#  donotusemore                    :boolean
#  issergicrecommended             :boolean
#  matriculemaya                   :string(255)
#  tiers_id                        :integer
#  plan_comptable_id               :integer
#  abonnementtyperelancecr         :string(255)
#  alertenewintervention           :boolean
#  gereboninterventionnum          :boolean
#  retourquestionnairesatisfaction :string(255)
#  workonpublicholiday             :boolean
#  workonsaturday                  :boolean
#  workonsunday                    :boolean
#

class Provider < ApplicationRecord
  self.table_name = 'fournisseur'
end
