# frozen_string_literal: true

# == Schema Information
#
# Table name: ecriture_comptable_immeuble
#
#  id                    :integer          not null, primary key
#  datemajaudit          :datetime
#  objversion            :integer
#  profilmajaudit        :string(255)
#  supprime              :boolean
#  userconnected         :string(255)
#  codeecriture          :string(255)
#  credit                :integer
#  credithtva            :integer
#  dateecheance          :datetime
#  dateecriture          :datetime
#  datefacture           :datetime
#  datesaisie            :datetime
#  debit                 :integer
#  debithtva             :integer
#  enattente             :boolean
#  libelle               :string(255)
#  montantrecuperation   :integer
#  numeropiece           :string(255)
#  numerofacture         :string(255)
#  referencemaya         :string(255)
#  situationfacture      :string(255)
#  typeecriture          :string(255)
#  visible               :boolean
#  chantier_id           :integer
#  recuperabilite_id     :integer
#  compte_credit_id      :integer
#  compte_debit_id       :integer
#  fournisseur_id        :integer
#  immeuble_id           :integer
#  nature_ecriture_id    :integer
#  profil_client_id      :integer
#  remise_bancaire_id    :integer
#  sinistre_id           :integer
#  cle_repartition_id    :integer
#  daterapprochement     :datetime
#  referencereglement    :string(255)
#  typereglement         :string(255)
#  fichier_id            :integer
#  matriculesalarie      :string(255)
#  numenregistrementmaya :integer
#  profilcreation        :string(255)
#  tva_id                :integer
#  fichier_lad_id        :integer
#

class AccountPlaceEntry < ApplicationRecord
  self.table_name = 'ecriture_comptable_immeuble'

  belongs_to :public_work         , foreign_key: :chantier_id
  belongs_to :lad_file            , foreign_key: :fichier_lad_id
  belongs_to :place               , foreign_key: :immeuble_id
  belongs_to :account_code_debit  , class_name: 'AccountCode',
                                    foreign_key: 'compte_debit_id'
  belongs_to :account_code_credit , class_name: 'AccountCode',
                                    foreign_key: 'compte_credit_id'
end
