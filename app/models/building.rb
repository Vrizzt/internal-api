# == Schema Information
#
# Table name: batiment
#
#  id                      :integer          not null, primary key
#  datemajaudit            :datetime
#  objversion              :integer
#  profilmajaudit          :string(255)
#  supprime                :boolean
#  userconnected           :string(255)
#  codebatiment            :string(255)
#  referencebatiment       :string(255)
#  immeuble_id             :integer
#  nom                     :string(255)
#  nombreetages            :integer
#  dateachevementtravaux   :datetime
#  dateconstruction        :datetime
#  type_ascenseur_id       :integer
#  isbbc                   :boolean
#  type_garage_velo_id     :integer
#  type_local_poussette_id :integer
#  type_parking_id         :integer
#  periode_construction_id :integer
#  adresse_id              :integer
#

class Building < ApplicationRecord
  self.table_name = 'batiment'

  belongs_to :place, foreign_key: :immeuble_id
  has_many :allocated_budgets, foreign_key: :batiment_id
end
