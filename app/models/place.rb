# frozen_string_literal: true

# == Schema Information
#
# Table name: immeuble
#
#  id                                 :integer          not null, primary key
#  datemajaudit                       :datetime
#  objversion                         :integer
#  profilmajaudit                     :string(255)
#  supprime                           :boolean
#  userconnected                      :string(255)
#  adresse1                           :string(255)
#  adresse2                           :string(255)
#  adresse3                           :string(255)
#  adresse4                           :string(255)
#  adresse5                           :string(255)
#  adresse6                           :string(255)
#  codeimmeuble                       :string(255)
#  isneuf                             :boolean
#  nom                                :string(255)
#  nombrelot                          :integer
#  nombremandatgestion                :integer
#  nombreniveaux                      :integer
#  societe                            :string(255)
#  type                               :string(255)
#  photo_principale_id                :integer
#  pole_copro_id                      :integer
#  pole_gerance_id                    :integer
#  tiers_sergic_assco_id              :integer
#  tiers_sergic_cptco_id              :integer
#  tiers_sergic_negoc_id              :integer
#  tiers_sergic_resre_id              :integer
#  tiers_sergic_syndic_id             :integer
#  datemiseenligne                    :datetime
#  descriptifimmeuble                 :text
#  famille                            :string(255)
#  isactive                           :boolean          default(FALSE)
#  statutactivation                   :string(255)
#  infos_id                           :integer
#  typemaya                           :integer
#  periode_appel_id                   :integer
#  datenominationsyndic               :datetime
#  moisclotureexercice                :integer
#  pack_id                            :integer
#  urlrenvoilotlocationetudiant       :string(255)
#  datederniereregul                  :datetime
#  adresse_id                         :integer
#  digicode                           :string(255)
#  isimmeublezonetendue               :boolean
#  nombrecourttennis                  :integer
#  codetarifregion                    :string(255)
#  withprivatepiscine                 :boolean
#  nomprogramme                       :string(255)
#  secteur_id                         :integer
#  immeuble_principal_id              :integer
#  numeroimmatriculationregistrecopro :string(255)
#  location                           :string(255)
#

class Place < ApplicationRecord
  self.table_name = 'immeuble'
  # disable STI
  self.inheritance_column = :_type_disabled

  has_many :fiscal_years, foreign_key: :immeuble_id
  has_many :account_place_entries, foreign_key: :immeuble_id
  has_many :account_codes, foreign_key: :immeuble_id
  has_many :buildings, foreign_key: :immeuble_id
  belongs_to :pack, foreign_key: :pack_id

  # ::AllocatedBudget.joins(:account_code, account_code: [:place], budget: [:fiscal_year, :budget_state])
  #                  .where(immeuble: { id: 25203 })
  #                  .where(budget_previsionnel: { supprime: 'false' })
  #                  .where(budget: { supprime: 'false' })
  #                  .where("#{Date.today.to_s} between exercice_comptable.datedebutexercice and exercice_comptable.datefinexercice")
  #                  .where(situation_budget: { codesituation: %w[A V] })

  # ::AccountPlaceEntry.joins(:account_code_debit, :place, place: [:fiscal_years])
  #                    .where(immeuble: {id: 25203})
  #                    .where(ecriture_comptable_immeuble: {supprime: 'false'})
  #                    .where(plan_comptable: {numcompte: 51200})
  #                    .where('dateecriture BETWEEN exercice_comptable.datedebutexercice AND exercice_comptable.datefinexercice')
  #                    .where("#{Date.today.to_s} BETWEEN exercice_comptable.datedebutexercice AND exercice_comptable.datefinexercice")

  def allocated_budgets(filters)
    QueryObject::AllocatedBudgetQuery.call(filters.merge(place_id: id))
  end

  def current_fiscal_year(date = Date.today.to_s)
    fiscal_years.where('? BETWEEN datedebutexercice AND
                       datefinexercice', date).first
  end

  # Return a hash with the data group by the filter asked
  def budgets_balance(current_fiscal_year = nil, code = nil, group_code = nil)
    result = Hash.new(0)
    budgets = ::AllocatedBudget.includes(:account_code, :budget, budget: [:fiscal_year], account_code: [:place]).where(immeuble: { id: 25203 }).where(plan_comptable: { numcompte: 60111 })

    # select allocated_budget of the current fiscal year
    if current_fiscal_year.present?
      budgets = budgets.where(budget: { exercice_comptable: { id: current_fiscal_year.id } })
    end

    # select allocated_budget of the code given (string: '60111')
    if code.present?
      budgets = budgets.where(plan_comptable: { numcompte: code })
    end

    # select allocated_budget of the group of code given (string: '601%')
    if group_code.present?
      # budgets = budgets.where(plan_comptable: { numcompte})
    end

    building.each do |l|
      code = l.allocated_budgets.map(&:account_code).numcompte
      amount = l.joins(:allocated_budgets).montantbudgetproprietaire
      result[code] += l.montantbudgetproprietaire
    end
    result
  end

  def account_code_entries(code)
    code_id = ::AccountCode.find_by(numcompte: code)&.id
    ::AccountPlaceEntry.where("immeuble_id = ? AND
                              (compte_debit_id = ? OR compte_credit_id = ?)",
                              id, code_id, code_id)
  end

  # return an hash with the credit, debit and balance amouts for each account
  def accounts_balance(date_start = nil, date_end = nil, code = nil, group_code = nil)
    balances = Hash.new { |h, k| h[k] = { 'credit' => 0, 'debit' => 0,
                                          'balance' => 0, 'nb_entries' => 0 } }

    balances['entries'] = { 'total'  => account_place_entries.count,
                            'sample' => 0 }

    if date_start.present? && date_end.present?
      balances['dates'] = { 'start' => clean_date(date_start),
                            'end'   => clean_date(date_end) }
    end
    entries = ::AccountPlaceEntry.includes(:account_code_debit, :account_code_credit)
                                 .where(immeuble_id: id)

    if date_start.present? && date_end.present?
      entries = entries.where("dateecriture >= '#{clean_date(date_start)}'
                               AND dateecriture <= '#{clean_date(date_end)}'")
    end
    if code.present?
      entries = entries.joins(:account_code_debit, :account_code_credit)
                       .where('plan_comptable.numcompte = ?', code)
    end
    if group_code.present?
      entries = entries.joins(:account_code_debit, :account_code_credit)
                       .where('plan_comptable.numcompte LIKE ?', group_code)
    end
    entries.each do |entry|
      way    = entry.debit.to_i.zero? ? 'credit' : 'debit'
      amount = way == 'credit' ? entry.send(way.to_sym) : -entry.send(way.to_sym)
      numcompte = entry.send("account_code_#{way}".to_sym).numcompte.to_sym

      balances[numcompte][way]          += amount
      balances[numcompte]['balance']    += amount
      balances[numcompte]['nb_entries'] += 1
      balances['entries']['sample']     += 1
    end
    # open("#{Rails.root}/data/#{Time.now.to_i}_accounts_balance.txt", 'w') { |l|
    #   l.puts balances }
    balances
  end

  private

  def clean_date(date)
    Date.strptime(date.to_s, '%Y-%m-%d').to_s
  end
end
