# frozen_string_literal: true

# == Schema Information
#
# Table name: plan_comptable
#
#  id                        :integer          not null, primary key
#  datemajaudit              :datetime
#  objversion                :integer
#  profilmajaudit            :string(255)
#  supprime                  :boolean
#  userconnected             :string(255)
#  cletantieme               :string(255)
#  codebatiment              :string(255)
#  codeplantypeofficiel      :string(255)
#  comptedebit               :integer
#  detaillebudget            :string(255)
#  isautocreated             :boolean
#  isnoticeable              :boolean
#  libellecomptelong         :string(255)
#  libellecomptereduit       :string(255)
#  libellesynthesecredit     :string(255)
#  libellesynthesedebit      :string(255)
#  nombrecaracterecompte     :integer
#  numcompte                 :string(255)
#  souscomptedebit           :string(255)
#  type_categorie_depense_id :integer
#  type_observatoire_cnab_id :integer
#  type_recuperabilite_id    :integer
#  type_usage_sous_compte_id :integer
#  caracteristique_id        :integer
#  immeuble_id               :integer
#  isplanstandard            :boolean
#  classeducompte            :integer
#

class AccountCode < ApplicationRecord
  self.table_name = 'plan_comptable'
  belongs_to :place, foreign_key: :immeuble_id
  has_many :allocated_budgets, foreign_key: :plan_comptable_id
  has_many :debit_account_place_entries,
             class_name: 'AccountPlaceEntry',
             foreign_key: 'compte_debit_id'
  has_many :credit_account_code_credit,
             class_name: 'AccountPlaceEntry',
             foreign_key: 'compte_credit_id'
end
