# frozen_string_literal: true

# == Schema Information
#
# Table name: exercice_comptable
#
#  id                       :integer          not null, primary key
#  datemajaudit             :datetime
#  objversion               :integer
#  profilmajaudit           :string(255)
#  supprime                 :boolean
#  userconnected            :string(255)
#  datedebutexercice        :datetime
#  datefinexercice          :datetime
#  datelimiteag             :datetime
#  libelle                  :string(255)
#  numeroexercice           :integer
#  immeuble_id              :integer
#  datecloturecomptes       :datetime
#  datecspreparatoire       :datetime
#  dateprevueagsaisie       :datetime
#  dateregulcharges         :datetime
#  datesortiecomptes        :datetime
#  profilcloturecomptes     :string(255)
#  profilcspreparatiore     :string(255)
#  profildateprevueagsaisie :string(255)
#  profilregulcharges       :string(255)
#  profilsortiecomptes      :string(255)
#  assemblee_generale_id    :integer
#

class FiscalYear < ApplicationRecord
  self.table_name = 'exercice_comptable'

  belongs_to :place, foreign_key: :immeuble_id
  has_many :budgets, foreign_key: :exercice_comptable_id

  scope :between, lambda { |date|
    where('? >= datedebutexercice AND ? <= datefinexercice', date, date)
  }
end
