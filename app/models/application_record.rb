# frozen_string_literal: true
require 'benchmark'

class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  # mesure exectution time for queries
  # action -> symbol
  # arg -> array of arguments
  # careful, eval give full acces to the system
  def benshmark(action, arg, option = :bmbm)
    Benchmark.send(option) do |x|
      x.report("#{action}:") { send(action.to_s, *arg) }
    end
  end
end
