# == Schema Information
#
# Table name: situation_budget
#
#  id             :integer          not null, primary key
#  datemajaudit   :datetime
#  objversion     :integer
#  profilmajaudit :string(255)
#  supprime       :boolean
#  userconnected  :string(255)
#  codesituation  :string(255)
#  libelle        :string(255)
#

class BudgetState < ApplicationRecord
  self.table_name = 'situation_budget'
  has_many :budgets
end
