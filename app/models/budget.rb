# frozen_string_literal: true

# == Schema Information
#
# Table name: budget
#
#  id                      :integer          not null, primary key
#  datemajaudit            :datetime
#  objversion              :integer
#  profilmajaudit          :string(255)
#  supprime                :boolean
#  userconnected           :string(255)
#  datecalculrealise       :datetime
#  datelastvalidation      :datetime
#  numerobudgetdeexercice  :integer
#  exercice_comptable_id   :integer
#  situation_budget_id     :integer
#  ismodifpresidentallowed :boolean
#

class Budget < ApplicationRecord
  self.table_name = 'budget'

  belongs_to :fiscal_year, foreign_key: :exercice_comptable_id
  belongs_to :budget_state, foreign_key: :situation_budget_id
  has_many :allocated_budgets
end
