# frozen_string_literal: true

# == Schema Information
#
# Table name: budget_previsionnel
#
#  id                        :integer          not null, primary key
#  datemajaudit              :datetime
#  objversion                :integer
#  profilmajaudit            :string(255)
#  supprime                  :boolean
#  userconnected             :string(255)
#  montantbudgetlocataire    :integer
#  montantbudgetproprietaire :integer
#  montantprochainappel      :integer
#  montantrealise            :integer
#  montanttheoriqueappele    :integer
#  tauxevolutionlastyear     :integer
#  batiment_id               :integer
#  budget_id                 :integer
#  cle_repartition_id        :integer
#  plan_comptable_id         :integer
#

class AllocatedBudget < ApplicationRecord
  self.table_name = 'budget_previsionnel'

  belongs_to :budget, foreign_key: :budget_id
  belongs_to :account_code, foreign_key: :plan_comptable_id
  belongs_to :building, foreign_key: :batiment_id
end
