# frozen_string_literal: true

module Eseis
  class AccountPlaceEntry
    include ActiveModel::Validations
    attr_accessor :provider_attributes,
                  :display_name,
                  :operation_date,
                  :amount,
                  :sergic_id,
                  :sergic_place_id,
                  :sergic_fiscal_year_id,
                  :account_code,
                  :sergic_provider_id,
                  :debit,
                  :credit,
                  :public_work_sergic_id,
                  :invoices_attributes

    def initialize( provider_attributes = '',
                    display_name = '',
                    operation_date = '',
                    amount = '',
                    sergic_id = nil,
                    sergic_place_id = nil,
                    sergic_fiscal_year_id = nil,
                    account_code = '',
                    sergic_provider_id = nil,
                    debit = '',
                    credit = '',
                    public_work_sergic_id = nil,
                    invoices_attributes = [])

      @provider_attributes   = provider_attributes
      @display_name          = display_name
      @operation_date        = operation_date
      @amount                = amount
      @sergic_id             = sergic_id
      @sergic_place_id       = sergic_place_id
      @sergic_fiscal_year_id = sergic_fiscal_year_id
      @account_code          = account_code
      @sergic_provider_id    = sergic_provider_id
      @debit                 = debit
      @credit                = credit
      @public_work_sergic_id = public_work_sergic_id
      @invoices_attributes   = invoices_attributes
    end

    validates :display_name,
              :operation_date,
              :amount,
              :sergic_id,
              :sergic_place_id,
              :sergic_fiscal_year_id,
              :account_code,
              :debit,
              :credit,
              :invoices_attributes,
              presence: true
  end
end
