# frozen_string_literal: true

module Eseis
  class FiscalYear
    include ActiveModel::Validations
    attr_accessor :display_name,
                  :start_date,
                  :end_date,
                  :current,
                  :sergic_place_id,
                  :sergic_id

    def initialize( display_name = '',
                    start_date = '',
                    end_date = '',
                    current = '',
                    sergic_place_id = nil,
                    sergic_id = nil)

      @display_name    = display_name
      @start_date      = start_date
      @end_date        = end_date
      @current         = current
      @sergic_place_id = sergic_place_id
      @sergic_id       = sergic_id
    end

    validates :display_name,
              :start_date,
              :end_date,
              :sergic_place_id,
              :sergic_id,
              presence: true
  end
end
