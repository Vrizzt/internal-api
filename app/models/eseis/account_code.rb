# frozen_string_literal: true

module Eseis
  class AccountCode
    include ActiveModel::Validations
    attr_accessor :display_name, :code, :sergic_id

    def initialize( display_name = '',
                    code = '',
                    sergic_id = nil)

      @display_name = display_name
      @code         = code
      @sergic_id    = sergic_id
    end

    validates :sergic_id, :code, :display_name, presence: true
  end
end
