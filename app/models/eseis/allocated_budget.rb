# frozen_string_literal: true

module Eseis
  class AllocatedBudget
    include ActiveModel::Validations
    attr_accessor :sergic_place_id,
                  :sergic_fiscal_year_id,
                  :account_code,
                  :allocated_amount,
                  :sergic_id

    def initialize( sergic_place_id = nil,
                    sergic_fiscal_year_id = nil,
                    account_code = '',
                    allocated_amount = '',
                    sergic_id = '')

      @sergic_place_id       = sergic_place_id
      @sergic_fiscal_year_id = sergic_fiscal_year_id
      @account_code          = account_code
      @allocated_amount      = allocated_amount
      @sergic_id             = sergic_id
    end

    validates :sergic_place_id,
              :sergic_fiscal_year_id,
              :account_code,
              :allocated_amount,
              :sergic_id,
              presence: true
  end
end
