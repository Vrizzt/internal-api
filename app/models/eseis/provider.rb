module Eseis
  class Provider
    include ActiveModel::Validations
    attr_accessor :display_name,
                  :sergic_id,
                  :sergic_update_time,
                  :sergic_slug

    def initialize(provider)
      @display_name = provider.nomentreprise
      @sergic_id = provider.id
      @sergic_update_time = provider.datemajaudit
      @sergic_slug = provider.matriculemaya
    end

    validates :display_name,
              :sergic_id,
              :sergic_update_time,
              :sergic_slug,
              presence: true
  end
end
