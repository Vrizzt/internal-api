# frozen_string_literal: true

# == Schema Information
#
# Table name: fichier_lad
#
#  id                           :integer          not null, primary key
#  datemajaudit                 :datetime
#  objversion                   :integer
#  profilmajaudit               :string(255)
#  supprime                     :boolean
#  userconnected                :string(255)
#  extension                    :string(255)
#  nomfichier                   :string(255)
#  nomfichierlad                :string(255)
#  taillefichier                :integer
#  nomfichierladavanteasyfolder :string(255)
#

class LadFile < ApplicationRecord
  self.table_name = 'fichier_lad'

  has_many :account_place_entries
end
