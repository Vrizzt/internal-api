# frozen_string_literal: true
# == Schema Information
#
# Table name: dataqueue_eseis_exo_comptable
#
#  id             :integer          not null, primary key
#  fiscal_year_id :integer
#  state          :string
#  action         :string
#  created_at     :datetime
#  updated_at     :datetime
#

module Dataqueue
  class EseisFiscalYear < ApplicationRecord
    self.table_name = 'dataqueue_eseis_exo_comptable'

    validates :fiscal_year_id, presence: true

    # State machine
    state_machine initial: :pending do
      state :pending, :in_progress, :done, :failed

      event :launch do
        transition pending: :in_progress
      end

      event :done do
        transition in_progress: :done
      end

      event :failed do
        transition %i[pending in_progress refused] => :failed
      end

      event :rescue do
        transition failed: :in_progress
      end

      event :refuse do
        transition in_progress: :refused
      end
    end
  end
end
