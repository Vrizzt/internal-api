# == Schema Information
#
# Table name: travaux
#
#  id                   :integer          not null, primary key
#  datemajaudit         :datetime
#  objversion           :integer
#  profilmajaudit       :string(255)
#  supprime             :boolean
#  userconnected        :string(255)
#  codetravaux          :string(255)
#  dateagconcernee      :datetime
#  datedebut            :datetime
#  datereception        :datetime
#  montant              :float
#  titre                :string(255)
#  etat_travaux_id      :integer
#  type_travaux_id      :integer
#  immeuble_id          :integer
#  listposition         :integer
#  cadencement          :string(255)
#  datearchivage        :datetime
#  datesoldecompte      :datetime
#  isinactive           :boolean
#  montantappele        :integer
#  montantcommande      :integer
#  montantfacture       :integer
#  topreductionfiscale  :boolean
#  compte_depense_id    :integer
#  compte_patrimoine_id :integer
#  compte_recette_id    :integer
#  descriptif           :text
#

class PublicWork < ApplicationRecord
  self.table_name = 'travaux'

  belongs_to :place, foreign_key: :immeuble_id
  has_many :account_place_entries
end
