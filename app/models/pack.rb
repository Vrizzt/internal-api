# frozen_string_literal: true

# == Schema Information
#
# Table name: pack
#
#  id                         :integer          not null, primary key
#  datemajaudit               :datetime
#  objversion                 :integer
#  profilmajaudit             :string(255)
#  supprime                   :boolean
#  userconnected              :string(255)
#  code                       :string(255)
#  commentaire                :string(255)
#  datedebutcommercialisation :datetime
#  datefincommercialisation   :datetime
#  isactif                    :boolean
#  issergicliability          :boolean
#  nomcommercial              :string(255)
#  versionvalideapplication   :string(255)
#  site                       :string(255)
#  googlesheetid              :string(255)
#  metierconcerne             :string(255)
#  urlsitepublic              :string(255)
#  emetteuremailclient        :string(255)
#

class Pack < ApplicationRecord
  self.table_name = 'pack'
  has_many :places
end
