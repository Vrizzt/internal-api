# frozen_string_literal: true

class RootController < ApiController
  def index
    head 200
  end
end
