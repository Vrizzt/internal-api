# frozen_string_literal: true

require 'rest-client'

class ApiClient
  attr_reader :client_id, :client_secret, :scope, :grant_type

  def initialize(client_id:, client_secret:, scope:, grant_type:, auth_uri:)
    @client_id = client_id
    @client_secret = client_secret
    @scope = scope
    @grant_type = grant_type
    @auth_uri = auth_uri
  end

  def post(base_uri, payload)
    RestClient.post(
      base_uri,
      payload,
      Authorization: "Bearer #{access_token}"
    )
  end

  def delete(base_uri)
    RestClient::Request.execute(
      method: :delete,
      url: base_uri,
      Authorization: "Bearer #{access_token}"
    )
  end

  def access_token
    payload = {
      client_id: client_id,
      scope: scope,
      client_secret: client_secret,
      grant_type: grant_type
    }

    @access_token ||= RestClient.post(@auth_uri, payload)
    JSON.parse(@access_token.body)['access_token']
  rescue RestClient::ExceptionWithResponse => e
    Appsignal.set_error(e)
  end
end
