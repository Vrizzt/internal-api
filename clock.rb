# frozen_string_literal: true

require 'clockwork'
require 'clockwork/database_events'
require_relative '../config/boot'
require_relative '../config/environment'

module Clockwork
  Clockwork.manager = DatabaseEvents::Manager.new

  error_handler do |err|
    Appsignal.send_exception(err)
  end

  every(1.minute, 'dataqueue.eseis_account_code') do
    # Dataqueue::EseisAccountCode.update_all(state: :pending)
    Dataqueue::EseisAccountCode.with_state(:pending).ids.each do |id|
      SyncEseisAccountCodeJob.perform_later(id)
    end
  end

  every(1.minute, 'dataqueue.eseis_account_place_entry') do
    # Dataqueue::EseisAccountPlaceEntry.update_all(state: :pending)
    Dataqueue::EseisAccountPlaceEntry.with_state(:pending).ids.each do |id|
      SyncEseisAccountPlaceEntryJob.perform_later(id)
    end
  end

  every(1.minute, 'dataqueue.eseis_allocated_budget') do
    # Dataqueue::EseisAllocatedBudget.update_all(state: :pending)
    Dataqueue::EseisAllocatedBudget.with_state(:pending).ids.each do |id|
      SyncEseisAllocatedBudgetJob.perform_later(id)
    end
  end

  every(1.minute, 'dataqueue.eseis_fiscal_year') do
    # Dataqueue::EseisFiscalYear.update_all(state: :pending)
    Dataqueue::EseisFiscalYear.with_state(:pending).ids.each do |id|
      SyncEseisFiscalYearJob.perform_later(id)
    end
  end
end
